from django.urls import path
from tasks.views import create_task, show_my_tasks, edit_task, delete_task, create_company, company_list, edit_company, delete_company, search_tasks, task_detail

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/edit/", edit_task, name="edit_task"),
    path("<int:id>/delete/", delete_task, name="delete_task"),
    path("createCompany/", create_company, name="create_company"),
    path("companies/", company_list, name="company_list"),
    path("company/<int:id>/edit/", edit_company, name="edit_company"),
    path("company/<int:id>/delete/", delete_company, name="delete_company"),
    path("searchTasks/", search_tasks, name="search_tasks"),
    path("<int:id>/detail/", task_detail, name="task_detail"),
]
