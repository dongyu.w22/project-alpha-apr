from django.db import models
from projects.models import Project
from django.contrib.auth.models import User


# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=255)
    notes = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    company = models.ManyToManyField(
        Company,
        blank=True,
        related_name="tasks",
    )
    cover_photo = models.ImageField(upload_to="cover_photos/", null=True, blank=True)

    def __str__(self):
        return self.name
