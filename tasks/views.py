from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm, TaskEditForm, CompanyForm, TaskSearchForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task, Company


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST, request.FILES)
        if form.is_valid():
            task = form.save(commit=False)
            task.owner = request.user
            task.save()
            form.save_m2m()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/tasks.html", context)


def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskEditForm(request.POST, request.FILES, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.save()
            form.save_m2m()
            return redirect("show_my_tasks")
    else:
        form = TaskEditForm(instance=task)

    context = {
        "task_object": task,
        "task_form": form,
    }
    return render(request, "tasks/edit.html", context)


def delete_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        task.delete()
        return redirect("show_my_tasks")
    return render(request, "tasks/delete.html")


def create_company(request):
    if request.method == "POST":
        form = CompanyForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("company_list")
    else:
        form = CompanyForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_company.html", context)


def company_list(request):
    companies = Company.objects.all()
    context = {
        "companies": companies,
    }
    return render(request, "tasks/companies.html", context)


def edit_company(request, id):
    company = get_object_or_404(Company, id=id)
    if request.method == "POST":
        form = CompanyForm(request.POST, instance=company)
        if form.is_valid():
            form.save()
            return redirect("company_list")
    else:
        form = CompanyForm(instance=company)

    context = {
        "company_object": company,
        "company_form": form,
    }
    return render(request, "tasks/edit_company.html", context)


def delete_company(request, id):
    company = get_object_or_404(Company, id=id)
    if request.method == "POST":
        company.delete()
        return redirect("company_list")
    return render(request, "tasks/delete_company.html")


def search_tasks(request):
    form = TaskSearchForm(request.GET or None)
    tasks = None
    if form.is_valid():
        company = form.cleaned_data['company']
        tasks = Task.objects.filter(company=company)

    context = {
        "form": form,
        "tasks": tasks,
    }
    return render(request, "tasks/search_tasks.html", context)


def task_detail(request, id):
    task = get_object_or_404(Task, id=id)
    context = {
        "task": task,
    }
    return render(request, "tasks/task_detail.html", context)
