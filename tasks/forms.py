from tasks.models import Task, Company
from django.forms import ModelForm
from django import forms


class TaskForm(ModelForm):
    company = forms.ModelMultipleChoiceField(queryset=Company.objects.all(), required=False)
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "company",
            "cover_photo",
        )
        widgets = {
            'start_date': forms.DateTimeInput(attrs={'type': 'datetime-local'}),
            'due_date': forms.DateTimeInput(attrs={'type': 'datetime-local'}),
        }


class TaskEditForm(forms.ModelForm):
    company = forms.ModelMultipleChoiceField(queryset=Company.objects.all(), required=False)
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "is_completed",
            "project",
            "assignee",
            "company",
            "cover_photo",
        )
        widgets = {
            'start_date': forms.DateTimeInput(attrs={'type': 'datetime-local'}),
            'due_date': forms.DateTimeInput(attrs={'type': 'datetime-local'}),
        }


class CompanyForm(ModelForm):

    class Meta:
        model = Company
        fields = (
            "name",
            "notes",
        )


class TaskSearchForm(forms.Form):
    company = forms.ModelChoiceField(queryset=Company.objects.all(), required=True, label="Select Company")
